import React from "react";
import PropTypes from "prop-types";

function Header({ icons }) {
  return (
    <header className="header">
      <div className="header__logo">- Brand sports shoes -</div>
      <div className="header__icons">{icons}</div>
    </header>
  );
}

Header.propTypes = {
  icons: PropTypes.element,
};

Header.defaultProps = {
  icons: null,
};

export default Header;
