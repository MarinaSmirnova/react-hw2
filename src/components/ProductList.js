import React from "react";
import PropTypes from 'prop-types';

function ProductList({ productCards }) {
  return <ul className="product__list">{productCards || "No products"}</ul>;
  
}

ProductList.propTypes = {
    productCards: PropTypes.node
  };

export default ProductList;
