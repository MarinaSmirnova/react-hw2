import React, { useState, useEffect } from "react";
import "./App.scss";
import ProductList from "./components/ProductList";
import ProductCard from "./components/ProductCard";
import Button from "./components/Button";
import Modal from "./components/Modal";
import Star from "./components/Star";
import Header from "./components/Header";
import { IoStarSharp } from "react-icons/io5";
import { IoIosCart } from "react-icons/io";
import IconWithInfo from "./components/IconWithInfo";

function App() {
  const [products, setProducts] = useState([]);
  const [openedModal, setOpenModal] = useState("None");
  const [activeProduct, setActiveProduct] = useState("");

  const checkLocalArray = (array) => {
    if (!localStorage.getItem(array)) {
      localStorage.setItem(array, JSON.stringify([]));
    }
  };

  checkLocalArray("productsInCart");
  checkLocalArray("likedProducts");

  const checkActiveProduct = (product) => {
    setActiveProduct(product);
  };

  const openModal = (modal) => {
    setOpenModal(modal);
  };

  const openConfirmModal = (product) => {
    openModal("Confirm");
    checkActiveProduct(product);
  };

  const checkStar = (product) => {
    if (checkProductInLikesArray(product)) {
      const likedProductsArray = JSON.parse(
        localStorage.getItem("likedProducts")
      );

      const newLikedProductsArray = likedProductsArray.filter(
        (productFromArray) => productFromArray.key !== product.key
      );

      localStorage.setItem(
        "likedProducts",
        JSON.stringify(newLikedProductsArray)
      );
    } else {
      addProductToArray(product, "likedProducts");
    }
  };

  const checkProductInLikesArray = (product) => {
    const likesArray = JSON.parse(localStorage.getItem("likedProducts"));
    const findProductInLiked = likesArray.find((productInLokalStarage) =>
      productInLokalStarage.key.includes(product.key)
    );

    return findProductInLiked ? true : false;
  };

  const takeLocalArrayLendth = (array) => {
    const localArray = JSON.parse(localStorage.getItem(array));
    return localArray.length;
  };

  const addProductToArray = (product, array) => {
    const productArray = JSON.parse(localStorage.getItem(array));

    productArray.push(product);
    const arrayInJSON = JSON.stringify(productArray);

    localStorage.setItem(array, arrayInJSON);

    setOpenModal("None");
    setActiveProduct("");
  };

  useEffect(() => {
    const featchDataOfProgucts = async () => {
      try {
        const response = await fetch("http://localhost:3000/products.json");
        const productsData = await response.json();

        const productsWithKeys = productsData.map((product) => ({
          ...product,
          key: product.article.toString(),
        }));

        setProducts(() => [...productsWithKeys]);
        return;
      } catch (e) {
        console.log("Помилка при виконанні fetch-запиту: ", e);
      }
    };

    featchDataOfProgucts();
  }, [checkProductInLikesArray]);

  return (
    <>
      <Header
        icons={
          <>
            <IconWithInfo
              icon={<IoStarSharp />}
              info={takeLocalArrayLendth("likedProducts")}
            />
            <IconWithInfo
              icon={<IoIosCart />}
              info={takeLocalArrayLendth("productsInCart")}
            />
          </>
        }
      />
      <ProductList
        productCards={
          <>
            {products.map((product) => (
              <ProductCard
                cardData={product}
                buttons={
                  <>
                    <Star
                      clickFunc={() => checkStar(product)}
                      type={
                        checkProductInLikesArray(product) ? "Painted" : "Empty"
                      }
                    />
                    <Button
                      clickFunc={() => openConfirmModal(product)}
                      text="Add to cart"
                    />
                  </>
                }
              />
            ))}
          </>
        }
      />

      {openedModal === "Confirm" && (
        <Modal
          modalSetter={setOpenModal}
          header="Add to cart"
          closeButton={true}
          text="Would you like to add this item to your cart?"
          actions={
            <>
              <Button
                clickFunc={() =>
                  addProductToArray(activeProduct, "productsInCart")
                }
                text="Yes"
              />
              <Button clickFunc={() => setOpenModal("None")} text="No" />
            </>
          }
        />
      )}
    </>
  );
}

export default App;
